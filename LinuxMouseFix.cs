﻿using UnityEngine;
using RimWorld;
using Verse;

namespace RimworldLinuxMouse
{
    //Not sure if ITab is an appropriate object type to extend, but the specific object type doesn't matter in this instance--we just need the assembly loaded to inject the MonoBehaviour.
    public class defLinFix : ITab
    {
        protected GameObject mouseUpdateObject;

        public defLinFix()
        {
			if (Application.platform == RuntimePlatform.LinuxPlayer)
            {
                Log.Message($"Running on Linux -- Enabling middle mouse camera panning workaround.");
                mouseUpdateObject = new GameObject("LinuxMouseFix");
                mouseUpdateObject.AddComponent<LinuxMouseFixBehavior>();
                Object.DontDestroyOnLoad(mouseUpdateObject);
            }
            else
            {
                Log.Message($"RimWorld is not running on Linux--disabling middle mouse camera panning workaround.");
            }
        }

        protected override void FillTab() { }
    }

    public class LinuxMouseFixBehavior : MonoBehaviour
    {
        /// <summary>
        /// The higher this value is, the slower the camera will pan. ~3 seems to match the drag speed in the Windows version.
        /// </summary>
        private static float dragControlRatio = 3f;
        private const int MIDDLE_MOUSE_BUTTON_CODE = 2;

        private bool
            midMouseDown, 
            dragging;

        private Vector3
            dragOrigin,
            delta,
            newCameraFocus,
            minVector = new Vector3(0, 0, 0), //always 0,0,0 since the map origin is at xz 0,0 and y (camera height) isn't going to be modified
            maxVector; //instantiate this on drag start since we don't know if the map exists when this script is attached to a game object.

        public void Update()
        {
            midMouseDown = Input.GetMouseButton(MIDDLE_MOUSE_BUTTON_CODE);

            if (!midMouseDown)
            {
                dragging = false;
                return;
            }

            if (isPanningWithKeyboard())
            {
                dragging = false;
                return;
            }

            if (!dragging)
            {
                dragging = true;
                dragOrigin = Camera.current.ScreenToWorldPoint(Input.mousePosition);
                maxVector = Current.Game.VisibleMap.Size.ToVector3();
                return; //skip the first cycle since there is no delta to calculate
            }
            
            delta = dragOrigin - Camera.current.ScreenToWorldPoint(Input.mousePosition);
            
            newCameraFocus = Clamp(Current.Camera.transform.localPosition + delta);;
			Current.CameraDriver.JumpToVisibleMapLoc(newCameraFocus);
            dragOrigin = Clamp(dragOrigin + (delta / dragControlRatio));
        }

        private bool isPanningWithKeyboard()
        {
            return Input.GetKey(KeyCode.W) ||
                   Input.GetKey(KeyCode.A) ||
                   Input.GetKey(KeyCode.S) ||
                   Input.GetKey(KeyCode.D) ||
                   Input.GetKey(KeyCode.UpArrow) ||
                   Input.GetKey(KeyCode.LeftArrow) ||
                   Input.GetKey(KeyCode.DownArrow) ||
                   Input.GetKey(KeyCode.RightArrow);
        }

        /// <summary>
        /// <para>Returns a vector with clamped X and Z coordinates.</para>
        /// <para>Since the camera height (Y coordinate) isn't set by Rimworld's CameraDriver.JumpTo function, we just set it to 1
        /// </summary>
        /// <param name="subject"></param>
        /// <returns></returns>
        private Vector3 Clamp(Vector3 subject)
        {
            return new Vector3(
                Mathf.Min(Mathf.Max(subject.x, minVector.x), maxVector.x),
                1f,
                Mathf.Min(Mathf.Max(subject.z, minVector.z), maxVector.z));
        }
    }
}
