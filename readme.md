What is this?
-------------

This is a RimWorld mod for Alpha 17 which restores the ability to hold 
down the middle mouse and pan the map on Linux.

Wait that's it?
---------------

Yep.